#!/usr/bin/env python3

import time

import numpy as np
import jax.numpy as jnp

import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

from smoofit.model import Variable,Process,Model,ChannelContrib
from smoofit.utils import build_1d_tunfold_matrix

if __name__ == "__main__":
    nBins = 50
    nBkg = 5
    nSig = 5
    nNuis = 5
    show = True

    sigs_yields = []
    gen_signal = jnp.linspace(0.2 * nBins//nSig, 10.*nBins//nSig, nBins)
    for i in range(nSig):
        this_signal = np.zeros((nBins,))
        nb = nBins//nSig
        this_signal[i*nb:(i+1)*nb] = gen_signal[i*nb:(i+1)*nb]
        this_signal = gaussian_filter(this_signal, 3, mode='constant')
        sigs_yields.append(this_signal)
        if show:
            plt.hist(np.arange(nBins), weights=this_signal, bins=nBins, histtype="step")
    if show:
        plt.show()
    sigs_yields = np.vstack(sigs_yields)

    gen_signal = gen_signal.reshape((nSig, nBins//nSig)).T
    gen_signal = jnp.sum(gen_signal, axis=0)
    if show:
        plt.hist(np.arange(nSig), weights=gen_signal, bins=nSig, histtype="step")
        plt.show()

    sigs = Process("sig", sub_procs=[f"sig{i}" for i in range(nSig)])
    bkgs = [ Process(f"bkg{i}") for i in range(nBkg) ]
    
    nuisance = Variable("nuisance", [0.]*nNuis, nuisance=True, sub_names=[f"nuis_{i}" for i in range(nNuis)])

    s_contrib = ChannelContrib("chan", sigs_yields, sub_procs=sigs.sub_procs)
    # s_contrib.add_lnN(nuisance, 1.05)
    s_contrib.add_shape_syst(nuisance, jnp.stack([ s_contrib.yields * 1.05 for _ in range(nNuis)], axis=0), jnp.stack([ s_contrib.yields * 0.95 for _ in range(nNuis)], axis=0))
    sigs.add_contrib(s_contrib)
    
    mu = Variable("mu", [1.]*nSig, lower_bound=[0.]*nSig, sub_names=[f"mu_{i}" for i in range(nSig)])
    sigs.scale_by(mu)

    for b in bkgs:
        b_contrib = ChannelContrib("chan", [100./nBkg]*nBins)
        # b_contrib.add_lnN(nuisance, 1.05)
        b_contrib.add_shape_syst(nuisance, jnp.stack([ b_contrib.yields * 1.05 for _ in range(nNuis)], axis=0), jnp.stack([ b_contrib.yields * 0.95 for _ in range(nNuis)], axis=0))
        b.add_contrib(b_contrib)

    model = Model()
    for proc in [sigs] + bkgs:
        model.add_proc(proc)
    model.correlate_nuisances((nuisance, 0), (nuisance, 1), -0.5)

    start = time.perf_counter()
    model.prepare()
    model.compile()
    tunfold = build_1d_tunfold_matrix(nSig)
    constr = lambda values,nObs,nGlob: -0.2 * jnp.sum((tunfold.dot(gen_signal * (values[mu.sub_idxs] - 1.)))**2)
    model.add_constraint(constr)
    print(f"Time to compile: {time.perf_counter() - start:.2f}s")

    true_distribution = jnp.linspace(1.3, 0.7, nSig)
    values = model.values_from_dict({mu: true_distribution})
    # Asimov
    obs = model.pred(values)

    start = time.perf_counter()

    best_fit = model.fit(obs)
    best_mus = best_fit.x[mu.sub_idxs]
    if not best_fit.success:
        print(best_fit)
        assert(0)
    print(f"best fit mus: {best_mus}")
    print("POI covariance matrix:")
    print(best_fit.hess_inv[mu.sub_idxs][:,mu.sub_idxs])

    pulls_up = []
    pulls_down = []
    for i in range(nNuis):
        pull_u,pull_d,_,_ = model.minos_bounds(nuisance, best_fit, obs, sub_idx=i)
        pulls_up.append(pull_u)
        pulls_down.append(pull_d)
        print(f"Nuisance {i}: pull = {pull_u:.2f}, {pull_d:.2f}")

    errs_up = []
    errs_down = []

    for sig_bin in range(nSig):
        print(f"Signal bin {sig_bin}")
        sig_bin_idx = mu.sub_idxs[sig_bin]

        best_mu = best_fit.x[sig_bin_idx]
        print(f"best mu: {best_fit.x[sig_bin_idx]:.2f} +- {jnp.sqrt(best_fit.hess_inv[sig_bin_idx,sig_bin_idx]):.2f}")
        up,down,_,_ = model.minos_bounds(mu, best_fit, obs, sub_idx=sig_bin)
        errs_up.append(up)
        errs_down.append(down)
        print(f"profiled best mu: {best_mu:.2f} +{up-best_mu:.2f} -{best_mu-down:.2f}")

        for i in range(nNuis):
            impact_up = model.profile({nuisance: ([pulls_up[i]], [i])}, best_fit, obs).x[sig_bin_idx] - best_mu
            impact_down = model.profile({nuisance: ([pulls_down[i]], [i])}, best_fit, obs).x[sig_bin_idx] - best_mu
            print(f"Nuisance {i}: impacts = {impact_up:.2f}, {impact_down:.2f}")

    true_distribution = true_distribution * gen_signal
    postfit_gen_signal = best_mus * gen_signal
    errs_up = gen_signal * jnp.array(errs_up)
    errs_down = gen_signal * jnp.array(errs_down)

    if show:
        bins = np.arange(nSig+1)
        plt.hist(bins[:-1], weights=gen_signal, bins=bins, histtype="step", label="pre fit")
        plt.hist(bins[:-1], weights=true_distribution, bins=bins, histtype="step", label="truth")
        plt.errorbar(bins[:-1] + 0.5, postfit_gen_signal, (errs_up-postfit_gen_signal, postfit_gen_signal-errs_down), fmt="o", label="post fit")
        plt.legend()
        plt.show()

    print(f"Time to run: {time.perf_counter() - start:.2f}s")
