#!/usr/bin/env python3

import time

import numpy as np
import jax.numpy as jnp

import matplotlib.pyplot as plt

from smoofit.model import Variable,Process,Model,ChannelContrib

if __name__ == "__main__":
    nBkg = 3

    sig = Process("sig")
    bkgs = [ Process(f"bkg_{i}") for i in range(nBkg) ]
    nuisances = [ Variable(f"nuis_{i}", 0., nuisance=True) for i in range(nBkg) ]
    lnN_both = Variable("lnN_both", 0., nuisance=True)

    s_contrib = ChannelContrib("def", np.array([50., 25., 5., 3., 2., 1., 1.]))
    s_contrib.register_sumw2(s_contrib.yields/10.)
    s_contrib.add_lnN(lnN_both, 1.05)
    sig.add_contrib(s_contrib)

    for b,v in zip(bkgs, nuisances):
        b_contrib = ChannelContrib("def", np.array([15., 25., 50., 25., 10., 5., 10.]))
        b_contrib.register_sumw2(b_contrib.yields/10.)
        b_contrib.add_shape_syst(v, up=np.array([18., 22., 60., 22., 15., 4., 12.]), down=np.array([12., 20., 40., 22., 5., 6., 12.]))
        b_contrib.add_lnN(lnN_both, 1.05)
        b.add_contrib(b_contrib)

    mu = Variable("mu", 1., lower_bound=0.)
    sig.scale_by(mu)

    model = Model()
    model.add_proc(sig)
    for bkg in bkgs:
        model.add_proc(bkg)

    start = time.perf_counter()
    model.enable_bblite()
    model.prepare()
    model.compile()
    print(f"Time to compile: {time.perf_counter() - start:.2f}s\n")

    print(f"Variables: {[v.name for v in model.vars]}")
    values = model.values_from_dict({mu: 1.})
    print(f"Variable values: {values}")
    obs = model.pred(model.values_from_dict({mu: 2.}))
    print(f"Observed: {obs}\n")

    print("Fitting...")
    start = time.perf_counter()
    best_fit = model.fit(obs)
    print(f"Time to fit: {time.perf_counter() - start:.2f}s\n")
    print(f"Best fit: {best_fit.x}")
    mu_idx = mu.sub_idxs[0]
    best_mu = best_fit.x[mu_idx]
    print(f"Best mu: {best_fit.x[mu_idx]:.2f} +- {np.sqrt(best_fit.hess_inv[mu_idx,mu_idx]):.2f}")
    up,down,_,__ = model.minos_bounds(mu, best_fit, obs)
    print(f"Profiled best mu: {best_mu:.2f} +{up-best_mu:.2f} -{best_mu-down:.2f}")
    direction = np.zeros((len(best_fit.x,)))
    direction[mu_idx] = up-best_mu
    up_stat = model.nll_crossings(direction, best_fit, obs).x[mu_idx]
    down_stat = model.nll_crossings(-direction, best_fit, obs).x[mu_idx]
    print(f"Stat.-only uncertainties on mu: +{up_stat-best_mu:.2f} -{best_mu-down_stat:.2f}\n")

    x = np.r_[
            np.linspace(1.7, 1.85, 5, endpoint=False),
            np.linspace(1.85, 2.15, 15, endpoint=False),
            np.linspace(2.15, 2.3, 5)]
    fix = []
    for m in x:
        vals = np.copy(best_fit.x)
        vals[mu_idx] = m
        fix.append(2 * (model.nll(vals, obs) - best_fit.fun))
    prof = [ 2 * (model.profile({mu: m}, best_fit, obs).fun - best_fit.fun) for m in x ]
    plt.plot(x, fix, label="Fixed")
    plt.plot(x, prof, label="Profiled")
    plt.plot([x[0], x[-1]], [1.]*2, "--")
    plt.plot([up, up], [0., 1.], "--")
    plt.plot([down, down], [0., 1.], "--")
    plt.plot([up_stat, up_stat], [0., 1.], "--")
    plt.plot([down_stat, down_stat], [0., 1.], "--")
    plt.legend()
    plt.gca().set_ylim([0.,1.5])
    plt.gca().set_ylabel("$-2 (\log L  -  \log L_0)$")
    plt.gca().set_xlabel("$\mu$")
    plt.show()

    x = np.linspace(1.5, 2.5, 25)
    y = np.linspace(-1., 1., 25)
    xv, yv = np.meshgrid(x, y)
    xyv = np.dstack((xv, yv))
    rest = best_fit.x[2:]
    z = np.apply_along_axis(lambda x: 2 * (model.nll(np.concatenate((x, rest)), obs) - best_fit.fun), -1, xyv).reshape((len(x), len(y)))
    plt.contourf(xv, yv, z, 100)
    plt.colorbar()
    plt.contour(xv, yv, z, np.array([0., 1, 4]), colors='r')
    plt.gca().set_ylabel(model.vars[1].name)
    plt.gca().set_xlabel("$\mu$")
    plt.show()

    print("Computing pulls and impacts...")
    for i,v in enumerate(model.nuisances):
        for j in range(v.dim):
            pull_u,pull_d,_,_ = model.minos_bounds(v, best_fit, obs, sub_idx=j)
            impact_up = model.profile({v: (pull_u, j)}, best_fit, obs).x[mu_idx] - best_mu
            impact_down = model.profile({v: (pull_d, j)}, best_fit, obs).x[mu_idx] - best_mu
            print(f"{v.name}-{j}: pull = {best_fit.x[v.sub_idxs[j]]:.2f} +{pull_u:.2f} {pull_d:.2f}, impacts = {impact_up:.2f}, {impact_down:.2f}")

    toy_nll = []
    toy_bf = []
    fit_time = []
    nToys = 100
    print(f"\nThrowing and fitting {nToys} toys...")
    nObs,nGlob = model.toy_pred(values, nToys),model.toy_glob(values, nToys)
    for i in range(nToys):
        start = time.perf_counter()
        best_fit = model.fit(nObs[i,:], nGlob[i,:], store_hess=False)
        fit_time.append(time.perf_counter() - start)
        toy_nll.append(float(best_fit.fun))
        toy_bf.append(float(best_fit.x[mu.sub_idxs[0]]))
    plt.hist(toy_nll, 10)
    plt.gca().set_xlabel("$L_0$")
    plt.show()
    plt.hist(toy_bf, 10)
    plt.gca().set_xlabel("$\hat{\mu}$")
    plt.show()
    plt.hist(fit_time, 10)
    plt.gca().set_xlabel("Fitting time (s)")
    plt.show()
