API reference
=============


.. currentmodule:: smoofit

.. autosummary::
    :toctree: stubs
    :template: class.rst

    model.Variable
    model.Process
    model.ChannelContrib
    model.Model
    model.MergedProcess

	:template:
    utils.build_1d_tunfold_matrix
    
	:template: class.rst
    utils.Dim6EFTMorphing
